import pytest
import json
from app import app


@pytest.fixture
def client(mocker):
    mocker.patch("app.send_to_firehose", return_value = None)
    with app.test_client() as client:
        yield client


def test_empty_query(client):
    rv = client.get('/')
    assert rv.status_code == 404


def test_track(client):
    correct = {
        "userId": "joe",
        "events": [
            {
                "eventName": "evName",
                "metadata": {},
                "timeStampUTC": 1611608429
            }
        ]
    }
    incorrect = {
        "userId": "joe2",
        "events": [
            {
                "eventName": "evName",
                "timeStampUTC": 1611608429
            }
        ]
    }

    rv = client.post(
        '/track',
        data = json.dumps(correct),
        content_type='application/json',
    )
    assert rv.status_code == 200

    rv = client.post(
        '/track',
        data = json.dumps(incorrect),
        content_type='application/json',
    )
    assert rv.status_code == 400


def test_alias(client):
    correct = {
        "newUserId": "joel",
        "originalUserId": "joe",
        "timeStampUTC": 1611608429
    }
    incorrect = {
        "newUserId": "joel",
        "originalUserId": "joe",
    }

    rv = client.post(
        '/alias',
        data = json.dumps(correct),
        content_type='application/json',
    )
    assert rv.status_code == 200

    rv = client.post(
        '/alias',
        data = json.dumps(incorrect),
        content_type='application/json',
    )
    assert rv.status_code == 400


def test_profile(client):
    correct = {
        "userId": "joel",
        "attributes": {
            "nickname": "joe",
        },
        "timeStampUTC": 1611608429
    }
    incorrect = {
        "userId": "joel",
        "attributes": {},
        "timeStampUTC": "yesterday"
    }

    rv = client.post(
        '/profile',
        data = json.dumps(correct),
        content_type='application/json',
    )
    assert rv.status_code == 200

    rv = client.post(
        '/profile',
        data = json.dumps(incorrect),
        content_type='application/json',
    )
    assert rv.status_code == 400
