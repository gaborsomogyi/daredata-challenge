from flask import Flask, jsonify, request
from marshmallow import ValidationError
import boto3
import json

from schemas import TrackSchema, AliasSchema, ProfileSchema


app = Flask(__name__)
boto3.setup_default_session(region_name='eu-west-1')


def send_to_firehose(data):
    firehose_name = "daredata-challenge-staging-stream"
    firehose_client = boto3.client('firehose')
    firehose_client.put_record(
        DeliveryStreamName=firehose_name,
        Record={'Data': json.dumps(data)}
    )


@app.route('/track', methods=['POST'])
def post_track():
    try:
        result = TrackSchema().load(request.json)
    except ValidationError as err:
        return jsonify(err.messages), 400
    send_to_firehose(result)
    return jsonify(success=True)


@app.route('/alias', methods=['POST'])
def post_alias():
    try:
        result = AliasSchema().load(request.json)
    except ValidationError as err:
        return jsonify(err.messages), 400
    send_to_firehose(result)
    return jsonify(success=True)


@app.route('/profile', methods=['POST'])
def post_profile():
    try:
        result = ProfileSchema().load(request.json)
    except ValidationError as err:
        return jsonify(err.messages), 400
    send_to_firehose(result)
    return jsonify(success=True)


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
