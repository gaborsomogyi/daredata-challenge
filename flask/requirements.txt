flask==1.1.2
marshmallow==3.10.0
flake8==3.8.4
pre-commit==2.9.3
pytest==6.2.1
boto3==1.16.59
pytest-mock==3.5.1
