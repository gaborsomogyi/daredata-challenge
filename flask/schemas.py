from marshmallow import Schema, fields


class EventSchema(Schema):
    eventName = fields.String(required=True)
    metadata = fields.Dict(required=True)
    timeStampUTC = fields.Integer(required=True)


class TrackSchema(Schema):
    userId = fields.String(required=True)
    events = fields.List(fields.Nested(EventSchema), required=True)


class AliasSchema(Schema):
    newUserId = fields.String(required=True)
    originalUserId = fields.String(required=True)
    timeStampUTC = fields.Integer(required=True)


class ProfileSchema(Schema):
    userId = fields.String(required=True)
    attributes = fields.Dict(required=True)
    timeStampUTC = fields.Integer(required=True)
