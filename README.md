# Daredata-challenge

This project creates a flask api that expects json data in a predefined format (see description), performs basic validation and passes in on to an S3 data lake via Amazon Firehose. 

## Challenge description

The original challenge description can be found in the description directory

## How to deploy

### Preparation steps

1. update the terraform backend s3 bucket in the `terraform/main.tf`
2. (optional) create an elastic IP if you want a fixed IP
3. Create an ssh key and place it in `ansible/ssh_keys/key.pem`

### Provision infrastructure

1. create a .tfvars file with the variables based on the `terraform/example.tfvars` file
2. execute `terraform init` in the `terraform` directory
3. execute `terraform apply -var-file=<.tfvars file just created>`

### Deploy the software using the ansibles

1. fill in the ansible variables found in `ansible/group_vars/all.yml` 
2. fill in the ip address or hostname of the host in the `ansible/hosts`
3. alternatively, use the `ansible/locals` dir to keep them private
4. fill in any ssh public keys for admins in the `ansible/ssh_keys/authorized_keys`
5. deploy with the following commands:

```bash
cd ansible
ansible-playbook setup-instance.yml
ansible-playbook deploy-app.yml
```

### Redeploying on code changes

If only the app code is changed, a simple you can redeploy the app with:
```bash
ansible-playbook deploy-app.yml
```

otherwise rerunning the terraforms and both ansibles may be necessary.


## Additional information

Before committing code, pre-commit runs and possibly reformats the code.
This setting can be fine-tuned in the `flask/.pre-commit-config.yaml` file.
