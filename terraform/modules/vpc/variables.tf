variable "app_name" {
  description = "Name of the app"
  type        = string
}

variable "stage" {
  description = "Stage of deployment"
  type        = string
}

variable "availability_zone" {
  description = "Availability zone for the subnet"
  type        = string
}
