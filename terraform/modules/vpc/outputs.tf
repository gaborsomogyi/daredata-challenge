output "subnet_id" {
  value = aws_subnet.main.id
}

output "security_group_ids" {
  value = [aws_security_group.tls_ssh.id]
}

output "internet_gateway" {
  value = aws_internet_gateway.gw
}
