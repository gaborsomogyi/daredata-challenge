output "public_ip" {
  # if there is an elastic ip added, return the associated ip, otherwise the generated one
  value = length(aws_eip_association.eip_assoc) > 0 ? aws_eip_association.eip_assoc[0].public_ip : aws_instance.web.public_ip
}
