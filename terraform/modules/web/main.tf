locals {
  common_tags = {
    Service     = var.app_name
    Environment = var.stage
  }
}

data "aws_ami" "ubuntu" {
  most_recent = true

  owners = ["099720109477"] # Canonical

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  filter {
    name   = "state"
    values = ["available"]
  }
}

resource "aws_instance" "web" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = var.instance_type
  iam_instance_profile = aws_iam_instance_profile.instance_profile.name

  subnet_id                   = var.subnet_id
  vpc_security_group_ids      = var.security_group_ids
  key_name                    = aws_key_pair.deploy_key.key_name
  associate_public_ip_address = true

  tags = merge(
    local.common_tags,
    {
      Name = "${var.app_name}-${var.stage}-web"
    }
  )
}

resource "aws_key_pair" "deploy_key" {
  key_name   = "${var.app_name}-${var.stage}-deploy-key"
  public_key = var.ssh_public_key
}


resource "aws_eip_association" "eip_assoc" {
  count = var.eip_id == null ? 0 : 1

  instance_id   = aws_instance.web.id
  allocation_id = var.eip_id

  depends_on = [var.internet_gateway]
}


resource "aws_iam_instance_profile" "instance_profile" {
  name = "${var.app_name}-${var.stage}-instance-profile"
  role = aws_iam_role.instance_role.name
}

resource "aws_iam_role_policy" "instance_role_policy" {
  name   = "${var.app_name}-${var.stage}-instance-role-policy"
  role   = aws_iam_role.instance_role.id

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "firehose:DeleteDeliveryStream",
        "firehose:PutRecord",
        "firehose:PutRecordBatch",
        "firehose:UpdateDestination"
      ],
      "Resource": [
          "${var.firehose_arn}"
      ]
    }
  ]
}
POLICY
}

resource "aws_iam_role" "instance_role" {
  name = "${var.app_name}-${var.stage}-instance-role"
  tags   = local.common_tags

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      }
    }
  ]
}
POLICY
}
