locals {
  common_tags = {
    Service     = var.app_name
    Environment = var.stage
  }
}

resource "aws_s3_bucket" "challenge" {
  bucket = "${var.app_name}-${var.stage}"
  acl    = "private"
  force_destroy = true # this allows to destroy the bucket even if there is data in it

  tags = merge(
    local.common_tags,
    {
      Name = "${var.app_name}-${var.stage}-bucket"
    }
  )
}

resource "aws_iam_role_policy" "firehose_policy" {
  name   = "${var.app_name}-${var.stage}-firehose-role-policy"
  role   = aws_iam_role.firehose_role.id

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "s3:AbortMultipartUpload",
        "s3:GetBucketLocation",
        "s3:GetObject",
        "s3:ListBucket",
        "s3:ListBucketMultipartUploads",
        "s3:PutObject"
      ],
      "Resource": [
        "${aws_s3_bucket.challenge.arn}",
        "${aws_s3_bucket.challenge.arn}/*"
      ]
    }
  ]
}
POLICY
}

resource "aws_iam_role" "firehose_role" {
  name = "${var.app_name}-${var.stage}-firehose-role"
  tags   = local.common_tags

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "firehose.amazonaws.com"
      }
    }
  ]
}
POLICY
}

resource "aws_kinesis_firehose_delivery_stream" "challenge_stream" {
  name        = "${var.app_name}-${var.stage}-stream"
  destination = "extended_s3"

  extended_s3_configuration {
    role_arn           = aws_iam_role.firehose_role.arn
    bucket_arn         = aws_s3_bucket.challenge.arn
    compression_format = "GZIP"
  }
  tags = local.common_tags
}
