output "bucket_name" {
  value = aws_s3_bucket.challenge.bucket
}

output "firehose_stream_name" {
  value = aws_kinesis_firehose_delivery_stream.challenge_stream.name
}

output "firehose_arn" {
  value = aws_kinesis_firehose_delivery_stream.challenge_stream.arn
}
