variable "app_name" {
  description = "Name of the app"
  type        = string
}

variable "stage" {
  description = "Stage of deployment"
  type        = string
}

variable "app_user" {
  description = "AWS IAM role to connect to firehose"
  type = string
}
