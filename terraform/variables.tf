variable "region" {
  description = "specifies the AWS region"
  default     = "eu-west-1"
}

variable "ssh_public_key" {
  description = "SSH public key"
}

variable "app_name" {
  description = "Name of the app"
  type        = string
}

variable "stage" {
  description = "Stage of deployment"
  type        = string
}

variable "instance_type" {
  description = "instance type for the web ec2"
}

variable "eip_id" {
  type        = string
  description = "elastic ip allocation"
  default     = null
}

variable "app_user" {
  description = "AWS IAM role to connect to firehose"
  type        = string
}
