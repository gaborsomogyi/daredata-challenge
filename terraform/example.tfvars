region         = "eu-west-1"
instance_type  = "t2.micro"
ssh_public_key = "ssh-rsa 12345 test"
app_name       = "appname"
stage          = "dev | staging | prod"
eip_id         = "eipalloc-1234" # optional
app_user       = "deploy_user_iam"
