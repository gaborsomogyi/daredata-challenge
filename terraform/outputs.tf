output "public_ip" {
  value       = module.web.public_ip
  description = "The public ip of the web instance"
}

output "bucket_name" {
  value       = module.firehose.bucket_name
  description = "S3 bucket name"
}

output "firehose_stream_name" {
  value       = module.firehose.firehose_stream_name
  description = "Firehose stream name"
}