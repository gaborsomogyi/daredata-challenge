terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
  required_version = "~> 0.14"
}

terraform {
  backend "s3" {
    encrypt = true
    bucket  = "terraform-daredata-challenge"
    key     = "tfstates/"
    region  = "eu-west-1"
  }
}

provider "aws" {
  profile = "default"
  region  = var.region
}


module "firehose" {
  source   = "./modules/firehose"
  app_user = var.app_user

  app_name = var.app_name
  stage    = var.stage
}

module "web" {
  source = "./modules/web"

  instance_type      = var.instance_type
  subnet_id          = module.vpc.subnet_id
  security_group_ids = module.vpc.security_group_ids
  ssh_public_key     = var.ssh_public_key

  eip_id           = var.eip_id
  internet_gateway = module.vpc.internet_gateway
  firehose_arn     = module.firehose.firehose_arn

  app_name = var.app_name
  stage    = var.stage
}

module "vpc" {
  source = "./modules/vpc"

  availability_zone = "${var.region}a"

  app_name = var.app_name
  stage    = var.stage
}
